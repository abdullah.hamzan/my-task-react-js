import React, { Component } from 'react'
import Button from './Button'
import '../styles/FormInput.css'
import axios from 'axios'
const baseUrl=`https://my-udemy-api.herokuapp.com/api/v1`


export default class FormInput extends Component {
    state ={
        text:""
    }
    change=e=>{

        this.setState({
            text:e.target.value
        })

    }

    submit=async(e)=>{
        e.preventDefault();
        const token=localStorage.getItem("token")
        if (this.state.text !==''){

            const newTodos={
                title:this.state.text
            }

            const res = await axios.post(`${baseUrl}/todo`,newTodos,{
                headers:{
                    "Content-Type":"application/json",
                    "Authorization":token
                }
            } );

            console.log("respons:",res)
            
            this.props.add(res.data.todo)
        }
        this.setState({text:""})
    }
    render() {
        return (
            <form className="cardInput" onSubmit={this.submit}>
                <input 
                style={{ marginRight:'-90px' }}
                value={this.state.text}
                placeholder="Add new task"
                onChange={this.change}
                
                />
                <Button name="Add" type="info" action={this.submit} />
                
            </form>
        )
    }
}
