
import axios from 'axios'
import React from 'react'
import '../styles/TodoItem.css'
import Button from './Button'
import SkeletonLoading from './SkeletonLoading'


const baseUrl=`https://my-udemy-api.herokuapp.com/api/v1`

export default function TodoItem({title,del,id,open,loading}) {
    const token=localStorage.getItem("token")
    const deletById=async(id)=>{

        console.log("id:",id)

        try {
            const res= await axios.delete(`${baseUrl}/todo/${id}`,{
                headers:{
                    "Content-Type":"application/json",
                     "Authorization":token
                }
            });

            console.log("response:",res)
        
            del(id)
    
            
        } catch (error) {

            console.log(error)
            
        }

     
    }

    return (<>
      {
          (loading) ?  (<SkeletonLoading />):(
     
        <div className="card">
         <p>{title}</p>
         <div className="button">
         <Button name="edit" type="success" action={()=>open(id,title)} />
         <div style={{ width:10 }}>

         </div>
         <Button name="delete" type="danger" action={()=>deletById(id)}/>
         </div>
        </div>
          )
}
        </>
    )
}
