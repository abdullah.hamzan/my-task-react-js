
import React from 'react'
import "../styles/Button.css"


function Button({name,type,action,load}) {
    return (
        <div >
          <button onClick={action} className={`cardButton-${type}`} >{(load)?"loading...":name}</button>
        </div>
    )
}

export default Button
