import React, { Component } from 'react'
import Button from './Button'
import '../styles/EditModal.css'

export default class EditModal extends Component {
    

    render() {

        const {edit,close,data,update,change}=this.props;
    
        if (edit){
        return (
            
            <div className="modal-container">
               <div className="modal-box">
                   <h3>Edit Task</h3>
                   <div className="input">
                       <input 
                       type="text"
                       value={data.title}
                       onChange={change}
                       
                       />
                   </div>
                   <div className="btn-group">
                       <Button 
                       name="update" 
                       type="success" 
                       action={update}
                       
                       />
                       <div style={{ width:'10px' }}>

                       </div>
                       <Button name="cancel" type="warning" action={close}  />
                   </div>

               </div>
            </div>
        )
        }else {
            
            return null
        }
    }
}
