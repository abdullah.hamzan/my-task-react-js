
import React from 'react'
import '../styles/Input.css'

const Input = ({placeholder,onChange,value,type}) => {
    return (
        <input 
        className="cardInput"
        type={type}
        placeholder={placeholder}
        onChange={onChange}
        value={value}
        
        
        />
    )
}

export default Input
