

import React, { Component } from 'react';
import { AuthProvider } from './context/auth';
import Routes from './router/Routes';


export default class App extends Component {
  render() {
    return (
      
      <AuthProvider>
     <Routes />
     </AuthProvider>
      
    );
  }
}
