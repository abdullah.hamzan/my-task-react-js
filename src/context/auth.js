import {createContext,useState} from 'react'

export const AuthContext=createContext()


export const AuthProvider=(props)=>{
    const [isAuthenticated,setAuth]=useState(false);
    const logginSuccess=()=>setAuth(true)
    const logginFailed=()=>setAuth(false)

    const loggout =()=>{
        localStorage.removeItem("token")
        setAuth(false);
    }
    return (
        <AuthContext.Provider value={{ isAuthenticated,logginSuccess,logginFailed,loggout }}>
            {
                props.children

            }
            
        </AuthContext.Provider>
    )


}