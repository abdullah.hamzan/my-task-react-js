
import React from 'react'

import {
    BrowserRouter as Router,
    Switch,
    Route,
  } from 'react-router-dom'
import Auth from '../views/Auth'
import Task from '../views/Task'
import PrivateRoute from './PrivateRoute'


const Routes = () => {
    return (
        <Router>
            <Switch>
            <Route  path="/" exact component={Auth} />
            <PrivateRoute  path="/task" component={Task}  />
           
            </Switch>
            
        </Router>
    )
}

export default Routes
