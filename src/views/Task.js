import React, { useEffect, useState,useContext } from "react"
import EditModal from "../components/EditModal"
import FormInput from "../components/FormInput"
import TodoItem from "../components/TodoItem"
import logo from '../logo.svg'
import '../App.css'
import axios from "axios"
import { AuthContext } from "../context/auth"
import Button from "../components/Button"

const baseUrl=`https://my-udemy-api.herokuapp.com/api/v1`
const Task =()=>{

  const {isAuthenticatedl,loggout}=useContext(AuthContext)

  const [todos,setTodos]=useState([
   

  ]);
  const [editData,setEditData]=useState(false);
  const [editTitle,setEditTitle]=useState({
    _id:'',
    title:""
  });

  const [load,setLoading]=useState(false)


 // delet data
  const deleteTodoItem=(id)=>{
    console.log('id:',id)
    console.log("delete oke")
  
    setTodos(todos.filter(item=>item._id !==id))
   

  }

  // tambah data sudah oke

  const addTask=data=>{
    // let id= todos.length+1
    // let newData= {
    //   id:id,
    //   title:data
    // }
    setTodos([...todos,data])
    
  }

  const openModal=(id,data)=>{
    setEditData(true);
    const editTitleData={
      _id:id,
      title:data
    };
    setEditTitle(editTitleData)
    console.log("oke dengan id:",id)
    console.log("oke dengan data:",data)
  }

  const closeModal=()=>{
    setEditData(false)
  
  }

  const setTitle=e=>{
    setEditTitle({
      ...editTitle,
      title:e.target.value
    })

   
  }

  const update =async()=>{

    const token=localStorage.getItem("token")

    try {

      const {_id,title}= editTitle
      const newdata={_id,title}
      const res= await axios.put(`${baseUrl}/todo/${_id}`,newdata,{
        headers:{
          "Content-Type":"application/json",
          "Authorization":token
        }
      })
      console.log("response after update data:",res)
      const editdata=res.data.todo
     
      const newTodos = todos
      newTodos.splice(_id,1,editdata)
      setTodos([...todos])
      setEditData(false)
      setEditTitle({
        _id:'',
        title:''
      })
      
    } catch (error) {

      console.log(error)
      
    }

  

  }

  const getDataApi= async()=>{
    setLoading(true)
    const token=localStorage.getItem("token")
    const res=await axios.get(`${baseUrl}/todo`,{
      headers:{
        "Content-Type":"application/json",
        "Authorization":token
      }
    })
    console.log("response:",res);

    setTodos(res.data.todos)
    setTimeout(()=>{


      setLoading(false)
    },2000)
    


  }

  useEffect(()=>{

    getDataApi()


  },[])

  
  return (
    <div className="app">
      <div className="logo">
        <img src={logo} width={130} height={115} alt="logo"/>
        <h3>Task List</h3>
        <Button  name="Logout" type="danger" action={loggout}/>
      </div>
      {
        
        todos.map((item,index)=>{
          return (
              <div className="list">
              <TodoItem 
              key={index}
              title={item.title} 
              id={item._id}
              open={openModal}
            
              del={deleteTodoItem}
              loading={load}

              
              
              />
              

              </div>

          )
        })
      }
      
    
      <div className="input-form">
        
      <FormInput add ={addTask} />

      </div>

      <EditModal 
      edit={editData} 
      close={closeModal}
      change={setTitle}
      data={editTitle}
      update={update}
      
      />
     
    </div>
  
  );
  
}

export default Task;
