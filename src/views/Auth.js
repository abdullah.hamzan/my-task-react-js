
import axios from 'axios'
import React, { useContext, useState } from 'react'
import Button from '../components/Button'
import Gap from '../components/Gap'
import Input from '../components/Input'
// import {useHistory} from 'react-router-dom'
import '../styles/Auth.css'
import { AuthContext } from '../context/auth'
import {Redirect} from 'react-router-dom'
const baseUrl=`https://my-udemy-api.herokuapp.com/api/v1`


const Auth = () => {


    const {isAuthenticated,logginSuccess,logginFailed,loggout} =useContext(AuthContext)

    console.log("isAuthenticated:",isAuthenticated)
    

    // const history=useHistory()
    

  


    const [register_error,setRegiterError]=useState("")
    const [name,setName]=useState("")
    const [email,setEmail]=useState("")
    const [password,setPassword]=useState("")
    const [newerror,setNewError]=useState("")
    const [isError,setIsError]=useState(false)

    const [login,setLogin]=useState(false)
    const [isLoading,setIsLoading]=useState(false)


    const isLogin=()=>setLogin(!login)


    const userLogin= async()=>{
        setIsLoading(true)
        const user={
            email,password
        }
        try {
            const res= await axios.post(`${baseUrl}/user/signin`,user)
            localStorage.setItem("token",res.data.token)
            console.log("res:",res)
            setEmail("")
            setPassword("")
            setIsLoading(false)
            logginSuccess()
            // history.push("/task")

            
        } catch (err) {
            setIsError(true)
            const msg_error=err.response.data.errors[0].msg
            setNewError(msg_error);
            setIsLoading(false)
            
            setEmail("")
            setPassword("")

            setTimeout(()=>{
                setIsError(false)
                logginFailed()
            },5000)
            
        }
    }


    const registerUser=async()=>{

        setIsLoading(true)
        const register={
            name,email,password
        }
        try {
            const res= await axios.post(`${baseUrl}/user/signup`,register)
            localStorage.setItem("token",res.data.token)
            console.log("res:",res)
            setName("")
            setEmail("")
            setPassword("")
            setIsLoading(false)
            // history.push("/task")

            
        } catch (err) {
            setIsError(true)
            console.log(err.response.data.errors)
            const msg_errorArray=err.response.data.errors
            setRegiterError(msg_errorArray);
            setIsLoading(false)
            setName("")
            setEmail("")
            setPassword("")

            setTimeout(()=>{
                setIsError(false)
            },5000)
            
        }


    }

    console.log("RegisterUserError:",register_error)

    if (isAuthenticated){

        return <Redirect to="/task"/>
    }


    return (
        <div className="cardContainer">
            <h3>{(login)? "Login":"Register"}</h3>
            {
                (login)? (
                    <>
                    <Input type="email" placeholder="masukan email" value={email} onChange={(e)=>setEmail(e.target.value)} />
                    <Gap height="12px" />
                    <Input  type="password"  placeholder="masukan password" value={password} onChange={(e)=>setPassword(e.target.value)} />
                    <Gap height="12px" />
                    </>

                ): (
                    <>
                      {
                        isError && <h5 style={{ marginBottom:'5px',color:'red',marginLeft:'30px' }}>{register_error[0].msg}</h5>
                    }
                    <Input type="text" placeholder="masukan name"  value={name} onChange={(e)=>setName(e.target.value)} />
                    {
                        isError && <h5 style={{ marginBottom:'5px',color:'red',marginLeft:'30px' }}>{register_error[1].msg}</h5>
                    }
                  
                    <Input type="email" placeholder="masukan email" value={email} onChange={(e)=>setEmail(e.target.value)}  />
                    {
                        isError && <h5 style={{ marginBottom:'5px',color:'red',marginLeft:'30px' }}>{register_error[2].msg}</h5>
                    }
                    <Gap height="12px" />
                    <Input type="password" placeholder="masukan password" value={password} onChange={(e)=>setPassword(e.target.value)}  />

                    <Gap height="12px" />
                    </>

                )
            }
            {
                (login)? <>  <div style={{ marginLeft:"2rem"}}>
                    {
                        isError && <h5 style={{ marginBottom:'10px',color:'red' }}>{newerror}</h5>
                    }
                <Button  type="success" name="Login" action={userLogin} load={isLoading}/>
                </div>
                <Gap height="12px" />
                <div style={{ marginLeft:"2rem" }}>
                    <h4>Belum punya akun ? silahkan <span onClick={isLogin} style={{ color:'green',cursor:'pointer' }}>Register</span></h4>
                </div>
                </>:  
                    <> <div style={{ marginLeft:"2rem"}}>
                    <Button  type="info" name="Register"load={isLoading} action={registerUser}/>
                    </div>
                    <Gap height="12px" />
                    <div style={{ marginLeft:"2rem" }}>
                        <h4>Jika punya akun ? silahkan <span onClick={isLogin} style={{ color:'green',cursor:'pointer' }}>Login </span></h4>
                    </div>
                    </>

            }

            
          
        </div>
        
    )
}

export default Auth
